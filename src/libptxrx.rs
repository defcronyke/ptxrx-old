#![feature(test)]

extern crate test;

#[cfg(test)]
mod tests {
    use super::*;
    use test::Bencher;

    #[bench]
    fn bench_xor_1000_ints(b: &mut Bencher) {
        b.iter(|| {
            (0..1000).fold(0, |a, b| a ^ b)
        });
    }

    #[bench]
    fn bench_xor_1000000_ints(b: &mut Bencher) {
        b.iter(|| {
            (0..1000000).fold(0, |a, b| a ^ b)
        });
    }

    #[bench]
    fn bench_xor_10000000_ints(b: &mut Bencher) {
        b.iter(|| {
            (0..10000000).fold(0, |a, b| a ^ b)
        });
    }

    #[bench]
    fn bench_xor_100000000_ints(b: &mut Bencher) {
        b.iter(|| {
            (0..100000000).fold(0, |a, b| a ^ b)
        });
    }
}