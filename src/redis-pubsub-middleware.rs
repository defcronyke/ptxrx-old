extern crate redis;
use crate::redis::Commands;
use std::time::Duration;

fn set_read_timeout(con: &mut redis::Connection) -> redis::RedisResult<()> {
    con.set_read_timeout(Some(Duration::new(5, 0)))
}

fn set_key(con: &mut redis::Connection) -> redis::RedisResult<()> {
    con.set("key", 33)
}

fn get_key(con: &mut redis::Connection) -> redis::RedisResult<String> {
    con.get("key").into()
}

fn main() {
    let client = redis::Client::open("redis://127.0.0.1/").unwrap();
    let mut con = client.get_connection().unwrap();
    set_read_timeout(&mut con).unwrap();
    set_key(&mut con).unwrap();
    let get_key_res = get_key(&mut con).unwrap();
    let get_key_res2 = get_key(&mut con).unwrap();
    println!("{},{}", get_key_res, get_key_res2);
}